import {
  Controller,
  Post,
  Body,
  HttpException,
  HttpStatus,
  HttpService,
  InternalServerErrorException,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Stream } from './interfaces/stream.interface';
import { Logger } from './logger/logger';
import { Microservice, ServiceLocator } from './config/service-locator';

@ApiTags('iot-integration')
@Controller('iot-integration/links')
export class IotIntegrationController {
  private readonly baseUrl: string;
  constructor(
    private readonly httpService: HttpService,
    private readonly serviceLocator: ServiceLocator,
    private readonly logger: Logger,
  ) {
    this.baseUrl = this.serviceLocator.getMicroserviceUrl(
      Microservice.iotIntegration,
      process.env.NODE_ENV,
    );
  }

  @Post('enable')
  async enableUserLinkInfo(@Body() userStream: Stream): Promise<void> {
    const logContext = `${IotIntegrationController.name}.${this.enableUserLinkInfo.name}`;

    try {
      await this.httpService
        .post(`${this.baseUrl}/camera-iot/enable`, userStream)
        .toPromise();

      this.logger.info('A new stream was created', logContext, userStream);
    } catch (error) {
      this.logger.error(
        'The stream link was not persisted on the system',
        null,
        `${IotIntegrationController.name}.${this.enableUserLinkInfo.name}`,
        error,
        { userStream },
      );

      throw new InternalServerErrorException(
        'Request to iot-integration microservice failed, try again later',
      );
    }
  }

  @Post('disable')
  async disableUserLinkInfo(@Body() userStream: Stream): Promise<void> {
    const logContext = `${IotIntegrationController.name}.${this.disableUserLinkInfo.name}`;

    try {
      await this.httpService
        .post(`${this.baseUrl}/camera-iot/disable`, userStream)
        .toPromise();

      this.logger.info('A new stream was created', logContext, userStream);
    } catch (error) {
      this.logger.error(
        'The stream link was not persisted on the system',
        null,
        logContext,
        error,
        { userStream },
      );

      new HttpException(
        'The link was not deleted of the system',
        HttpStatus.EXPECTATION_FAILED,
      );
    }
  }
}
