import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { Logger } from './logger/logger';

const logger = new Logger('Main');

async function bootstrap(): Promise<void> {
  const app = await NestFactory.create(AppModule);

  const options = new DocumentBuilder()
    .setTitle('Ipet - API Gateway')
    .setDescription('The api gateway to the ipet microservice backend')
    .setVersion('1.0')
    .build();

  const document = SwaggerModule.createDocument(app, options);

  SwaggerModule.setup('api', app, document);

  const port = process.env.PORT || 8080;

  await app.listen(port, () => {
    logger.info(`Api Gateway is listening on port ${port}`);
  });

  // just a dummy commit for the video
}

bootstrap();
