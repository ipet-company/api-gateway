export interface PostData {
    userId: string,
    description: string,
    fileId: string,
    date: Date
}