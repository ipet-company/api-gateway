import { mock, instance, verify, when, anything, resetCalls } from 'ts-mockito';
import { Logger } from './logger/logger';
import { IotIntegrationController } from './iot-integration.controller';
import { of } from 'rxjs';
import { Stream } from './interfaces/stream.interface';
import { HttpService } from '@nestjs/common';
import { ServiceLocator } from './config/service-locator';

describe('iot-integration.unit', () => {
  const mockedHttpService: HttpService = mock(HttpService);
  const mockedServiceLocator: ServiceLocator = mock(ServiceLocator);

  const streamTest: Stream = { userId: 'testUser', url: 'testUrl' };

  when(mockedHttpService.post(anything(), anything())).thenReturn(of(null));

  when(
    mockedServiceLocator.getMicroserviceUrl(anything(), anything()),
  ).thenReturn('baseUrl');

  const mockedLogger: Logger = mock(Logger);

  let iotIntegrationController: IotIntegrationController;

  beforeAll(() => {
    iotIntegrationController = new IotIntegrationController(
      instance(mockedHttpService),
      instance(mockedServiceLocator),
      instance(mockedLogger),
    );
  });

  describe('enableUserLinkInfo() test', () => {
    it('should enable and log', async () => {
      await iotIntegrationController.enableUserLinkInfo(streamTest);

      verify(
        mockedHttpService.post('baseUrl/camera-iot/enable', streamTest),
      ).once();
      verify(mockedLogger.info(anything(), anything(), anything())).once();

      resetCalls(mockedLogger);
    });
  });

  describe('disableUserLinkInfo() test', () => {
    it('should log and return "testUrl"', async () => {
      await iotIntegrationController.disableUserLinkInfo(streamTest);

      verify(
        mockedHttpService.post('baseUrl/camera-iot/disable', streamTest),
      ).once();

      verify(mockedLogger.info(anything(), anything(), anything())).once();

      resetCalls(mockedLogger);
    });
  });
});
