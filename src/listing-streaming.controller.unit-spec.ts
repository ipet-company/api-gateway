import { expect } from 'chai';
import { mock, instance, when, verify, anything } from 'ts-mockito';
import { ListingStreamingController } from './listing-streaming.controller';
import { Logger } from './logger/logger';
import { of } from 'rxjs';
import { HttpService } from '@nestjs/common';
import { ServiceLocator } from './config/service-locator';

describe('Streaming-Listing-Controller.unit', () => {
  const mockedLogger: Logger = mock(Logger);
  const mockedHttpService: HttpService = mock(HttpService);
  const mockedServiceLocator: ServiceLocator = mock(ServiceLocator);
  let listingStreamingController: ListingStreamingController;
  when(mockedHttpService.get(anything())).thenReturn(
    of({
      data: 'testUrl',
      status: null,
      statusText: null,
      headers: null,
      config: null,
    }),
  );

  beforeAll(() => {
    listingStreamingController = new ListingStreamingController(
      instance(mockedHttpService),
      instance(mockedServiceLocator),
      instance(mockedLogger),
    );
  });

  describe('getUserPrivateStreamLink() test', () => {
    it('should log and return "testUrl"', async () => {
      const url = await listingStreamingController.getUserPrivateStreamLink(
        'testUser',
      );

      verify(mockedLogger.info(anything(), anything(), anything())).once();
      expect(url).to.equal('testUrl');
    });
  });
});
