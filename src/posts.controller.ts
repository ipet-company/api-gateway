import {
  Controller,
  Get,
  Param,
  Post,
  Body,
  Put,
  Delete,
  HttpService,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Logger } from './logger/logger';
import { PostData } from './interfaces/post.interface';
import { Microservice, ServiceLocator } from './config/service-locator';

@ApiTags('posts')
@Controller('posts')
export class PostsController {
  private readonly baseUrl: string;
  constructor(
    private readonly httpService: HttpService,
    private readonly serviceLocator: ServiceLocator,
    private readonly logger: Logger,
  ) {
    this.baseUrl = this.serviceLocator.getMicroserviceUrl(
      Microservice.post,
      process.env.NODE_ENV,
    );
  }

  @Get('by-user/:userId')
  async getUserPosts(@Param('userId') userId: string): Promise<PostData> {
    this.logger.info(
      'Get User Posts request received',
      `${PostsController.name}.${this.getUserPosts.name}`,
      { userId },
    );

    return (
      await this.httpService
        .get(`${this.baseUrl}/posts/by-user/${userId}`)
        .toPromise()
    ).data;
  }

  @Get()
  async getAllPosts(): Promise<PostData[]> {
    this.logger.info(
      'Get All Posts request received',
      `${PostsController.name}.${this.getUserPosts.name}`,
    );

    return (await this.httpService.get(`${this.baseUrl}/posts`).toPromise())
      .data;
  }

  @Post()
  async createPost(@Body() postData: PostData): Promise<PostData> {
    this.logger.info(
      'Create User Post request received',
      `${PostsController.name}.${this.getUserPosts.name}`,
    );

    return (
      await this.httpService.post(`${this.baseUrl}/posts`, postData).toPromise()
    ).data;
  }

  @Put(':postId')
  async updatePost(@Param('postId') postId: string, @Body() postData: PostData): Promise<PostData> {
    this.logger.info(
      'Create User Post request received',
      `${PostsController.name}.${this.getUserPosts.name}`,
    );

    return (
      await this.httpService.put(`${this.baseUrl}/posts/${postId}`, postData).toPromise()
    ).data;
  }

  @Delete(':postId')
  async deletePost(@Param('postId') postId: string): Promise<void> {
    this.logger.info(
      'Delete User Post request received',
      `${PostsController.name}.${this.getUserPosts.name}`,
    );

    await this.httpService
      .delete(`${this.baseUrl}/posts/${postId}`)
      .toPromise();
  }
}
