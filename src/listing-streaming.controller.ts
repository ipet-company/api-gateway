import {
  Controller,
  Get,
  Param,
  HttpService,
  InternalServerErrorException,
} from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { Logger } from './logger/logger';
import { Microservice, ServiceLocator } from './config/service-locator';
import { response } from 'express';

@ApiTags('streaming')
@Controller('streaming/listing')
export class ListingStreamingController {
  private readonly baseUrl: string;
  constructor(
    private readonly httpService: HttpService,
    private readonly serviceLocator: ServiceLocator,
    private readonly logger: Logger,
  ) {
    this.baseUrl = this.serviceLocator.getMicroserviceUrl(
      Microservice.streaming,
      process.env.NODE_ENV,
    );
  }

  @Get(':userId')
  async getUserPrivateStreamLink(
    @Param('userId') userId: string,
  ): Promise<string> {
    this.logger.info(
      `User Private stream link was requested`,
      `${ListingStreamingController.name}.${this.getUserPrivateStreamLink.name}`,
      { userId },
    );

    try {
      const response = await this.httpService
        .get(`${this.baseUrl}/streaming/private-link/${userId}`)
        .toPromise();

      return response.data;
    } catch (error) {
      this.logger.error(
        `Request to streaming microservice failed`,
        null,
        `${ListingStreamingController.name}.${this.getUserPrivateStreamLink.name}`,
        error,
        { userId, response },
      );

      throw new InternalServerErrorException(
        'Request to streaming microservice failed, try again later',
      );
    }
  }
}
