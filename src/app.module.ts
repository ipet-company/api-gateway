import { Module, HttpModule } from '@nestjs/common';
import { ListingStreamingController } from './listing-streaming.controller';
import { IotIntegrationController } from './iot-integration.controller';
import { Logger } from './logger/logger';
import { PostsController } from './posts.controller';
import { ServiceLocator } from './config/service-locator';

@Module({
  imports: [
    HttpModule.register({
      timeout: 5000,
      maxRedirects: 5,
    }),
  ],
  controllers: [
    ListingStreamingController,
    IotIntegrationController,
    PostsController,
  ],
  providers: [ServiceLocator, Logger, String],
})
export class AppModule {}
