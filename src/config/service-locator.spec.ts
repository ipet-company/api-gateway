import { Microservice, ServiceLocator } from './service-locator';

describe('serviceLocator.unit', () => {
  const serviceLocator = new ServiceLocator();
  const testCasesDevelopmentEnvironment = [
    [Microservice.iotIntegration, 'http://localhost:8810'],
    [Microservice.streaming, 'http://localhost:8820'],
    [Microservice.post, 'http://localhost:8830'],
  ];

  const testCasesDevelopmentProduction = [
    [
      Microservice.iotIntegration,
      'https://ipet-iot-microservice.herokuapp.com',
    ],
    [
      Microservice.streaming,
      'https://ipet-streaming-microservice.herokuapp.com',
    ],
    [Microservice.post, 'https://ipet-post-microservice.herokuapp.com'],
  ];

  describe('getMicroserviceUrl()', () => {
    describe('when environment is production', () => {
      describe.each(testCasesDevelopmentProduction)(
        'when the given the microservice is %p:',
        (microservice, expectedUrl) => {
          it(`map object should return url: ${expectedUrl}`, () => {
            const result = serviceLocator.getMicroserviceUrl(
              microservice as Microservice,
              'production',
            );
            expect(result).toEqual(expectedUrl);
          });
        },
      );
    });

    describe('environment is everything else', () => {
      describe.each(testCasesDevelopmentEnvironment)(
        'when the given the microservice is %p:',
        (microservice, expectedUrl) => {
          it(`map object should return url: ${expectedUrl}`, () => {
            const result = serviceLocator.getMicroserviceUrl(
              microservice as Microservice,
              'anything',
            );
            expect(result).toEqual(expectedUrl);
          });
        },
      );
    });
  });
});
