import { Injectable } from '@nestjs/common';

export enum Microservice {
  streaming = 'streaming',
  iotIntegration = 'iotIntegration',
  post = 'post',
}

@Injectable()
export class ServiceLocator {
  getMicroserviceUrl(microservice: Microservice, environment: string): string {
    const hostAndPortByMicroserviceName: Record<Microservice, string> = {
      [Microservice.iotIntegration]:
        environment === 'production'
          ? 'https://ipet-iot-microservice.herokuapp.com'
          : 'http://localhost:8810',
      [Microservice.streaming]:
        environment === 'production'
          ? 'https://ipet-streaming-microservice.herokuapp.com'
          : 'http://localhost:8820',
      [Microservice.post]:
        environment === 'production'
          ? 'https://ipet-post-microservice.herokuapp.com'
          : 'http://localhost:8830',
    };

    return hostAndPortByMicroserviceName[microservice];
  }
}
