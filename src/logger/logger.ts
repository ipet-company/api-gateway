import * as Rollbar from 'rollbar';
import {
  Logger as NestLogger,
  LoggerService,
  Injectable,
} from '@nestjs/common';

@Injectable()
export class Logger implements LoggerService {
  private readonly rollbar: Rollbar;
  private readonly nestLogger: NestLogger;
  private readonly microservice = 'api-gateway';

  constructor(context?: string) {
    this.rollbar =
      process.env.NODE_ENV !== 'test'
        ? new Rollbar({
            accessToken: 'af5d5897f72f43e38c6fc0f75ac661e8',
            captureUncaught: true,
            captureUnhandledRejections: true,
            environment: process.env.NODE_ENV,
            logLevel: 'info',
            payload: {
              server: {
                branch: 'master',
                root:
                  process.env.NODE_ENV === 'production'
                    ? 'app/dist/main'
                    : 'E:\\Projects\\Ipet\\api-gateway\\', // temporary for development
              },
            },
          })
        : null;

    this.nestLogger = new NestLogger(context);
  }
  log(message: unknown, context?: string): void {
    this.nestLogger.log(message, context);
  }
  warn(message: unknown, context?: string): void {
    this.nestLogger.warn(message, context);
  }
  debug?(message: unknown, context?: string): void {
    this.nestLogger.debug(message, context);
  }
  verbose?(message: unknown, context?: string): void {
    this.nestLogger.verbose(message, context);
  }

  info(message: string, context?: string, payload?: {}): void {
    this.rollbar?.info(message, {
      microservice: this.microservice,
      context,
      ...payload,
    });

    this.nestLogger.log(message, context);
  }

  error(
    message: string,
    trace?: string,
    context?: string,
    error?: Error,
    payload?: {},
  ): void {
    this.rollbar?.error(message, error, {
      microservice: this.microservice,
      context,
      ...payload,
    });

    this.nestLogger.error(message, error.stack, context);
  }
}
