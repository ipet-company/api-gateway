FROM node:10.15.3 as dependencies

WORKDIR /app

COPY package*.json ./

RUN npm install

COPY . ./

RUN npm run build


FROM node:10.15.3-alpine

WORKDIR /app

COPY --from=dependencies /app ./

CMD npm run start:prod





